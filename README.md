# Spirograph

Explore the infinite and mesmerizing universe of epicycloïds.

https://en.wikipedia.org/wiki/Epicycloid

The 0.1.0 version of this project was 100% made
during a one-hour improvised tutorial available here:
https://www.youtube.com/watch?v=opSVdJawlMY

![An example epicycloid with its construction circles](demo.gif)


## How to run

1. Download Godot `3.2.1`. (or later?)
2. Open `project.godot`.


## Demo

Watch how it looks like:

https://www.youtube.com/embed/7fVHJcrVOZI


## Builds

- http://antoine.goutenoir.com/games/spirograph
- …
