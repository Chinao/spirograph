extends Node2D


# https://ggcode.school


var screen_size = OS.get_screen_size()


var circle_color := Color.aquamarine
var radius_color := Color.greenyellow
var point_color := Color.pink
var spiro_color := Color.plum
var max_spiro_points := pow(2, 13) # + 1   #-> OOOOPS


var circles = [
	
	{
		'radius': 30.0,          # % of viewport
		'rotation_speed': -0.1,  # turns/s
	},
	{
		'radius': 16.0,          # % of viewport
		'rotation_speed': 0.4,   # turns/s
	},
	{
		'radius': 4.0,          # % of viewport
		'rotation_speed': 0.4,  # turns/s
	},
	
	# …
]


var dynamic_angle := 0.0
#var spiro_points := Array()


var poli
var poli_mat
func _ready():
	poli_mat = ShaderMaterial.new()
	poli_mat.shader = preload("res://club.shader")
	
	poli = Polygon2D.new()
##	poli.set_polygon(spiro_points)
#	poli.material = poli_mat
	add_child(poli)


func _draw():
	draw_spiro(circles)


func draw_spiro(_circles):
	var viewport_size = get_viewport().get_visible_rect().size
	var viewport_side = min(viewport_size.x, viewport_size.y)
	var spiro_points := Array()
#	var dynamic_angle := 0.0
	
	for i in range(max_spiro_points):
		
		var dynamic_angle = i * TAU / 60.0
		var center = viewport_size / 2.0
		
	#	draw_point(center, 2.0, point_color)
		
		for circle in _circles:
			
			var previous_center = center
			
	#		draw_circle_not_disk(
	#			center,
	#			circle['radius'],
	#			circle_color
	#		)
			
			center = get_point_on_circle(
				center,
				viewport_side * 0.01 * circle['radius'],
				dynamic_angle * circle['rotation_speed']
			)
			
	#		draw_line(previous_center, center, radius_color)
	#		draw_point(center, 2.0, point_color)
		
		
		spiro_points.append(center)
#		var spiro_size = spiro_points.size()
#		if spiro_size < max_spiro_points:
#		else:
#			spiro_color = Color.blanchedalmond
	
#	if spiro_size > 1:  # draw_polyline will raise an error otherwise
	assert(spiro_points.size() > 1)
	draw_polyline_custom(spiro_points)

func draw_polyline_custom(spiro_points):
	draw_polyline(spiro_points, spiro_color, 3, true)
	
	# 2 ** 11
#	var spiro_colors = PoolColorArray()
#	var n = spiro_points.size() * 1.0
#	for i in range(n):
#		spiro_colors.append(Color(i/n, 1.0, 1.0, 1.0))
#	draw_polyline_colors(spiro_points, spiro_colors, 4, true)


func draw_polyline_custom2(spiro_points):
#	if poli:
#		return
	var poli_mat_tmp = ShaderMaterial.new()
#	var poli_mat_tmp = CanvasItemMaterial.new()
	poli_mat_tmp.shader = preload("res://club.shader")
	poli_mat_tmp.set_shader_param('color', Color.chartreuse)
	
#	if poli:
#		poli.get_parent().remove_child(poli)
#		poli.free()
	
#	poli = Polygon2D.new()
	poli.set_material(poli_mat_tmp)
	poli.set_polygon(spiro_points)
#	poli.set_polygons(spiro_points)
#	poli.material = poli_mat
#	add_child(poli)


# 666 999 = 666 x 999 + 666 + 999


var time = 0.0
func _process(delta):
	time += delta
#	dynamic_angle += TAU * 0.016
#	circles[0]['rotation_speed'] = 0.1 + 0.01 * sin(time * TAU * 0.01)
	circles[1]['rotation_speed'] = 0.3 + 0.1 * sin(time * TAU * 0.005)
	circles[2]['rotation_speed'] = 0.4 + 0.4 * sin(time * TAU * 0.0025)
#	circles[3]['rotation_speed'] = -0.1 + 0.1 * sin(time * TAU * 0.01)
	update()


func get_point_on_circle(center, radius, angle):
	return center + radius * Vector2(cos(angle), sin(angle))


func draw_circle_not_disk(center, radius, color=Color.white):
	draw_arc(center, radius, 0, TAU, 42, color)


func draw_point(at_position, radius=2.0, color=Color.white):
	draw_circle(at_position, radius, color)









