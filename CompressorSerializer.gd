# We're having issues when using class_name, somehow.
# Help us fix them, or just preload this script, that works ;)
#class_name CompressorSerializer


# Help serializing things to short strings.
# Short strings are easy to exchange between players.


# 256 characters that most fonts can print (at least Godot's can)
# 1F 8B 08 <- because of GZIP
# <  փ  ⋅     let's make it awesome
# This is used by the *_pretty methods to yield even shorter strings.
const EXCHANGE_TOKEN_CHARS : String = \
	"Ξ" + \
	"#§Ӝ!&¡¤⋅Ֆµ¿" + \
	"϶աբգդեզըթժիլխծկձճմյ<չպջռվտրӡնքֆշ" + \
	"ΓΔΘΛ%ΠΣΥΦΧΨΩΫάέήΰαβδεζηθλμνξοπρςστυφχψωϋύϏϐϑϒϓϔϕϖϗϘϙϚϛϜϝϞϟϠϡϢϣϤϥϦϧϨϩϪϫϬϭϮϯϰϱϲϳϴϵ" + \
	"$123456789abcdeփfghijklmnopqrstuvwxyz" + \
	"ԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕ" + \
	"@ABCDEFGHIJKLMNOPQRSTUVWXYZ" + \
	"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞß"


static func compress_string(s:String) -> String:
	"""
	String s : utf8, usually `JSON.print(mydata)`.
	Outputs a string containing only hexadecimal, uppercase characters,
	that is matching the regex `^[A-F0-9]*$`.
	"""
	var bytes : PoolByteArray = s.to_utf8()
	var bytes_count : int = bytes.size()
	var compressed_bytes : PoolByteArray = bytes.compress(File.COMPRESSION_GZIP)
	# TODO: append our byte count (maybe later, see note below)
	var compressed_str : String = ""
	for byte in compressed_bytes:
		compressed_str += "%02X" % byte
	
	return compressed_str


static func decompress_string(s:String) -> String:
	"""
	String s :
		utf8, usually an exchange token.
		MUST match the regex `^[A-F0-9]*$`.
	"""
	var compressed_bytes := PoolByteArray()
	for i in range(0, s.length(), 2):
		var byte = ("0x"+s.substr(i, 2)).hex_to_int()  # oh god why
		compressed_bytes.append(byte)
	compressed_bytes = PoolByteArray(compressed_bytes)
	
	# 2097152 = 1<<21 (totally arbitrary length, 1<<31 won't work)
	# note: if the expected output size is too low, nothing is returned at all
	# workaround: store the length in the byte sequence and retrieve it here
	var bytes : PoolByteArray = compressed_bytes.decompress(
		2097152, File.COMPRESSION_GZIP
	)
	if not bytes.size():
		printerr("Could not decompress string of size %d : `%s`." % [
			s.length(), s
		])
	
	return bytes.get_string_from_utf8()


static func compress_string_pretty(s:String) -> String:
	"""
	String s : utf8, usually `JSON.print(mydata)` or `var2str(mydata)`.
	"""
	var bytes = s.to_utf8()
	var bytes_count = bytes.size()
	var compressed_bytes = bytes.compress(File.COMPRESSION_GZIP)
	# Let's append our byte count (later, see note below)
	var compressed_str = ""
	for byte in compressed_bytes:
		compressed_str += EXCHANGE_TOKEN_CHARS.substr(byte, 1)

	return compressed_str


static func decompress_string_pretty(s:String) -> String:
	"""
	String s : utf8, usually an exchange token
	"""
	var compressed_bytes = PoolByteArray()
	for i in range(0, s.length()):
		var byte = EXCHANGE_TOKEN_CHARS.find(s[i])
		assert(byte > -1)  # ouch, if this happens
		compressed_bytes.append(byte)
	compressed_bytes = PoolByteArray(compressed_bytes)
	
	# 2097152 = 1<<21 (totally arbitrary length, 1<<31 won't work)
	# note: if the expected output size is too low, nothing is returned at all
	# workaround: store the length in the byte sequence and retrieve it here
	var bytes = compressed_bytes.decompress(2097152, File.COMPRESSION_GZIP)
	if not bytes.size():
		printerr("Could not decompress string of size %d : `%s`." % [
			s.length(), s
		])
	
	return bytes.get_string_from_utf8()


static func compress_resource(r:Resource) -> String:
	var s = get_resource_as_string(r)
	return compress_string(s)


static func compress_resource_pretty(r:Resource) -> String:
	var s = get_resource_as_string(r)
	return compress_string_pretty(s)


static func get_resource_as_string(r:Resource) -> String:
	var s : String = ""
	# Since I could not find an API to export the Resource to String,
	# we're exporting to a temporary file and then reading from it. -_-
	# This is definitely not thread-safe, and very painful to look at.
	var tmp_filepath = "user://tmp_resource_stup_crou_wesh.tres"
	var saved : int = ResourceSaver.save(tmp_filepath, r)
	if OK != saved:
		printerr("Cannot write to `%s`. (Error %d)" % [tmp_filepath, saved])
		return s
	
	var file := File.new()
	var opened : int = file.open(tmp_filepath, File.READ)
	if OK == opened:
		s = file.get_as_text()
	else:
		printerr("Cannot open `%s`. (Error %d)" % [tmp_filepath, opened])
		return s
	
	var dir = Directory.new()
	dir.remove(tmp_filepath)
	
	return s

