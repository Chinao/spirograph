extends Node2D


# A customizable, animated spirograph.
# Draws an epicycloïd and animates its properties.
# Made for https://ggcode.school
# spirograph-plum-rebeccapurple <3


var spiro_color := Color.plum


var spirograph:SpirographResource


var circle_properties = {
	'radius':           {'min': 0.0, 'max': 50.0,  'unit': "% of viewport"},
	'radius_variation': {'min': 0.0, 'max': 50.0,  'unit': "% of viewport"},
	'radius_frequency': {'min': 0.0, 'max': 1.618, 'unit': "loops per second"},
	'phase':            {'min': 0.0, 'max': TAU,   'unit': "radians"},
	'phase_variation':  {'min': 0.0, 'max': TAU,   'unit': "radians"},
	'phase_frequency':  {'min': 0.0, 'max': 1.618, 'unit': "loops per second"},
	'speed':            {'min':-1.0, 'max': 1.0,   'unit': "turns per second"},
	'speed_variation':  {'min': 0.0, 'max': 0.25,  'unit': "turns per second"},
	'speed_frequency':  {'min': 0.0, 'max': 0.1,   'unit': "loops per second"},
}


export var boomerang_flat_strength = 2.0
export var boomerang_expo_strength = 1.05
export var boomerang_elasticity = 0.33

var boomerangs = [
	{
		"action": "implode_c00",
		"property": "circle_00_radius",
		"flat_strength_multiplier": 0.3,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c01",
		"property": "circle_01_radius",
		"flat_strength_multiplier": 0.3,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c02",
		"property": "circle_02_radius",
		"flat_strength_multiplier": 0.3,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c03",
		"property": "circle_03_radius",
		"flat_strength_multiplier": 0.3,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c00_variation",
		"property": "circle_00_radius_variation",
		"flat_strength_multiplier": 1.0,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c01_variation",
		"property": "circle_01_radius_variation",
		"flat_strength_multiplier": 0.3,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c02_variation",
		"property": "circle_02_radius_variation",
		"flat_strength_multiplier": 0.3,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c03_variation",
		"property": "circle_03_radius_variation",
		"flat_strength_multiplier": 0.3,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c00_frequency",
		"property": "circle_00_radius_frequency",
		"flat_strength_multiplier": 0.003,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c01_frequency",
		"property": "circle_01_radius_frequency",
		"flat_strength_multiplier": 0.003,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c02_frequency",
		"property": "circle_02_radius_frequency",
		"flat_strength_multiplier": 0.003,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "implode_c03_frequency",
		"property": "circle_03_radius_frequency",
		"flat_strength_multiplier": 0.003,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c00",
		"property": "circle_00_phase",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c01",
		"property": "circle_01_phase",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c02",
		"property": "circle_02_phase",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c03",
		"property": "circle_03_phase",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c00_variation",
		"property": "circle_00_phase_variation",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c01_variation",
		"property": "circle_01_phase_variation",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c02_variation",
		"property": "circle_02_phase_variation",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c03_variation",
		"property": "circle_03_phase_variation",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c00_frequency",
		"property": "circle_00_phase_frequency",
		"flat_strength_multiplier": 0.01,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c01_frequency",
		"property": "circle_01_phase_frequency",
		"flat_strength_multiplier": 0.01,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c02_frequency",
		"property": "circle_02_phase_frequency",
		"flat_strength_multiplier": 0.01,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "rotate_c03_frequency",
		"property": "circle_03_phase_frequency",
		"flat_strength_multiplier": 0.01,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c00",
		"property": "circle_00_speed",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c01",
		"property": "circle_01_speed",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c02",
		"property": "circle_02_speed",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c03",
		"property": "circle_03_speed",
		"flat_strength_multiplier": 0.03,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c00_variation",
		"property": "circle_00_speed_variation",
		"flat_strength_multiplier": 0.001,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c01_variation",
		"property": "circle_01_speed_variation",
		"flat_strength_multiplier": 0.001,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c02_variation",
		"property": "circle_02_speed_variation",
		"flat_strength_multiplier": 0.001,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c03_variation",
		"property": "circle_03_speed_variation",
		"flat_strength_multiplier": 0.001,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c00_frequency",
		"property": "circle_00_speed_frequency",
		"flat_strength_multiplier": 0.001,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c01_frequency",
		"property": "circle_01_speed_frequency",
		"flat_strength_multiplier": 0.001,
		"expo_strength_multiplier": 1.0,
	},
	{
		"action": "spin_c02_frequency",
		"property": "circle_02_speed-frequency",
		"flat_strength_multiplier": 1.0,
		"expo_strength_multiplier": 1.0,
	},	
	{
		"action": "spin_c03_frequency",
		"property": "circle_03_speed_frequency",
		"flat_strength_multiplier": 0.001,
		"expo_strength_multiplier": 1.0,
	},
]


onready var menu = $'../Menu'
onready var sliders_container = $"../Menu/VBoxContainer"
onready var time_label = $"../Menu/VBoxContainer/HBoxContainer/TimeLabel"


func _ready():
	self.spirograph = preload("res://spirographs/default.tres")
#	self.spirograph = SpirographResource.new()
	build_cache_from_resource_spirograph(self.spirograph)
	export_to_string(self.spirograph)
	
	for circle_idx in range(self.spirograph.amount_of_circles):
		for property_name in circle_properties.keys():
			create_menu_slider(circle_idx, property_name)
		sliders_container.add_child(HSeparator.new())


func _draw():
	draw_spiro(self.spirograph)


var time_speed := 1.0
var time := 60.0
var is_time_processing = true


func _process(delta):
	# delta ~= 0.01666s when not laggy (at 60 FPS)
	if self.is_time_processing:
		self.time += delta * self.time_speed
		self.time_label.set_text("%.2f" % self.time)
	process_live_inputs()
	update()


func _input(event):
	if Input.is_action_just_pressed("toggle_menu"):
		menu.visible = not menu.visible
	if Input.is_action_just_pressed("toggle_fullscreen"):
		OS.window_fullscreen = not OS.window_fullscreen
	if Input.is_action_just_pressed("clear"):
		self.spirograph.clear()
		


func create_menu_slider(circle_idx:int, property_name:String):
	var slider := HSlider.new()
	slider.set_name("slider_%02d_%s" % [circle_idx, property_name])
	slider.set_scrollable(false)
	slider.set_min(circle_properties[property_name]['min'])
	slider.set_max(circle_properties[property_name]['max'])
	slider.set_step(
		0.001 * (circle_properties[property_name]['max'] - circle_properties[property_name]['min'])
	)
#	slider.set_exp_ratio(true)
	slider.set_value(circles_cache[circle_idx][property_name])
	slider.set_tooltip("Circle %02d %s (%s)" % [
		circle_idx,
		property_name.capitalize(),
		circle_properties[property_name]['unit']
	])
	slider.connect("value_changed", self, "_on_slider_value_changed", [circle_idx, property_name])
	sliders_container.add_child(slider)


func update_menu(spiro:SpirographResource):
	for k in range(spiro.amount_of_circles):
		for property_name in self.circle_properties.keys():
			var sld_name = "slider_%02d_%s" % [k, property_name]
			var sld = self.sliders_container.find_node(sld_name, true, false)
			if sld:
				sld.set_value(spiro.get("circle_%02d_%s" % [k, property_name]))
			else:
				printerr("Cannot find node `%s`." % sld_name)
#			circle_cache[parameter_name] = spiro.get("circle_%02d_%s" % [k, parameter_name])


var circles_cache = Array()
func build_cache_from_resource_spirograph(spiro:SpirographResource):
	circles_cache.clear()
	for k in range(spiro.amount_of_circles):
		var circle_cache = Dictionary()
		for parameter_name in circle_properties.keys():
			circle_cache[parameter_name] = spiro.get("circle_%02d_%s" % [k, parameter_name])
		circles_cache.append(circle_cache)
	#print("Built cache from spirograph resource.")


func build_spiro(spiro:SpirographResource):
	var viewport_size = get_viewport().get_visible_rect().size
	var viewport_side = min(viewport_size.x, viewport_size.y)
	var spiro_points := Array()
#	var dynamic_angle := 0.0
	
	for i in range(spiro.amount_of_samples):
		
		var dynamic_angle = i * TAU / 60.0 * spiro.draw_speed
		var center = viewport_size * spiro.initial_center
		
		for k in range(spiro.amount_of_circles):
			
			var circle_radius = self.circles_cache[k]['radius']
			var circle_radius_variation = self.circles_cache[k]['radius_variation']
			var circle_radius_frequency = self.circles_cache[k]['radius_frequency']
			var circle_phase = self.circles_cache[k]['phase']
			var circle_phase_variation = self.circles_cache[k]['phase_variation']
			var circle_phase_frequency = self.circles_cache[k]['phase_frequency']
			var circle_speed = self.circles_cache[k]['speed']
			var circle_speed_variation = self.circles_cache[k]['speed_variation']
			var circle_speed_frequency = self.circles_cache[k]['speed_frequency']
			
			if null == circle_radius:
				continue
			
			var t = time * TAU * spiro.animation_speed
			center = get_point_on_circle(
				center,
				viewport_side * 0.01 * (circle_radius + circle_radius_variation * sin(t * circle_radius_frequency)),
				(circle_phase + circle_phase_variation * sin(t * circle_phase_frequency)) + dynamic_angle * (circle_speed + circle_speed_variation * sin(t * circle_speed_frequency))
			)
		
		spiro_points.append(center)
	
	return spiro_points


func draw_spiro(spiro:SpirographResource):
	var spiro_points = build_spiro(spiro)
	assert(spiro_points.size() > 1)
	
	if spiro.color_gradient:
		self.spiro_color = spiro.color_gradient.interpolate((cos(time * 0.1) + 1.0) / 2.0)
	draw_polyline_custom(spiro_points)


func draw_polyline_custom(spiro_points):
	draw_polyline(spiro_points, self.spiro_color, 3, true)
	
	# 2 ** 11
#	var spiro_colors = PoolColorArray()
#	var n = spiro_points.size() * 1.0
#	for i in range(n):
#		spiro_colors.append(Color(i/n, 1.0, 1.0, 1.0))
#	draw_polyline_colors(spiro_points, spiro_colors, 4, true)


func get_point_on_circle(center, radius, angle):
	return center + radius * Vector2(cos(angle), sin(angle))


func draw_circle_not_disk(center, radius, color=Color.white):
	draw_arc(center, radius, 0, TAU, 42, color)


func draw_point(at_position, radius=2.0, color=Color.white):
	draw_circle(at_position, radius, color)


const CompressorSerializer = preload("res://CompressorSerializer.gd")


func pickle_spiro(spiro:SpirographResource):
	var pickle = Array()
	for k in range(spiro.amount_of_circles):
		for property_name in self.circle_properties.keys():
			pickle.append(spiro.get("circle_%02d_%s" % [k, property_name]))
	return pickle


func unpickle_spiro(pickle, into_spiro:SpirographResource):
	var i = 0
	var pl = pickle.size()
	for k in range(into_spiro.amount_of_circles):
		for property_name in self.circle_properties.keys():
			if i >= pl:
				printerr("Looks like the pickle is too short!")
				continue
			into_spiro.set(
				"circle_%02d_%s" % [k, property_name],
				pickle[i]
			)
			i += 1


func export_to_string(spiro:SpirographResource):
	# Compression is only worth it for bigger blobs of data than what we have.
#	var s = CompressorSerializer.compress_resource_pretty(spiro)
#	var s = CompressorSerializer.compress_string(
#		var2str(pickle_spiro(spiro))
#	)
#	prints("Spirograph Exchange String:", s)
	
	# Instead, let's just use plain var2str
	var s = var2str(pickle_spiro(spiro))
	var exchange_label = $"../Menu/VBoxContainer/ExchangeStringLineEdit"
	exchange_label.set_text(s)



var initial_values = Dictionary()  # property:String => initial_value:float

func process_live_inputs():
	
#	var freeze_parameters = false
	if Input.is_action_just_pressed("freeze"):
#		initial_values.clear()
		for parameter in initial_values:
			initial_values[parameter] = self.spirograph.get(parameter)
#		freeze_parameters = true
	
	for boomerang in boomerangs:
		var action = boomerang['action']
		var property = boomerang['property']
		if Input.is_action_just_pressed(action):
			if not initial_values.has(property):
				initial_values[property] = self.spirograph.get(property)
		
		if Input.is_action_pressed(action):
			var new_value = self.spirograph.get(property)
			if Input.is_action_pressed("inverter"):
				new_value /= boomerang_expo_strength * boomerang['expo_strength_multiplier']
				new_value -= boomerang_flat_strength * boomerang['flat_strength_multiplier']
			else:
				new_value *= boomerang_expo_strength * boomerang['expo_strength_multiplier']
				new_value += boomerang_flat_strength * boomerang['flat_strength_multiplier']
			self.spirograph.set(property, new_value)
		else:
			if initial_values.has(property):
				var fallback_intention = (
					initial_values[property] - \
					self.spirograph.get(property)
				)
				
				self.spirograph.set(property,
					self.spirograph.get(property) + \
					boomerang_elasticity * fallback_intention
				)
				
				if abs(fallback_intention) < 0.001:
					initial_values.erase(property)
	
	build_cache_from_resource_spirograph(self.spirograph)
	update_menu(self.spirograph)






var _changes_coming_from_export = false
func _on_slider_value_changed(value, circle_idx:int, property_name:String):
	#prints("Changed", property_name, value)
	self.circles_cache[circle_idx][property_name] = value
	self.spirograph.set("circle_%02d_%s" % [circle_idx, property_name], value)
	if not _changes_coming_from_export:
		export_to_string(self.spirograph)


func _on_SaveButton_pressed():
	$'../Menu/SaveFileDialog'.popup_centered()


func _on_LoadButton_pressed():
	$'../Menu/LoadFileDialog'.popup_centered()


func _on_SourceButton_pressed():
	OS.shell_open("https://framagit.org/ggcode.school/spirograph")


func _on_SaveFileDialog_file_selected(path:String):
	print("Saving to `%s`…" % path)
	var saved = ResourceSaver.save(path, self.spirograph)
	if OK == saved:
		print("Saved to `%s`." % path)
	else:
		printerr("Failed to save to `%s`." % path)


func _on_LoadFileDialog_file_selected(path:String):
	print("Loading from `%s`…" % path)
	var spirou = load(path)
	if not spirou:  # then fantasio
		printerr("Cannot load spirograph file `%s`." % path)
	else:
		print("Loaded from `%s`." % path)
		self.spirograph = spirou
		build_cache_from_resource_spirograph(self.spirograph)
		update_menu(self.spirograph)


func _on_ExchangeStringLineEdit_text_changed(new_text):
#	prints("Export String changed !", new_text)
	var spiro_pickle = str2var(new_text)
	if spiro_pickle and spiro_pickle is Array:
		unpickle_spiro(spiro_pickle, self.spirograph)
		build_cache_from_resource_spirograph(self.spirograph)
		_changes_coming_from_export = true
		update_menu(self.spirograph)
		_changes_coming_from_export = false
	else:
		printerr("Cannot load string `%s`." % [new_text])


func _on_TimeControlSlider_value_changed(value):
	self.time_speed = value


func _on_TimeLabel_focus_entered():
	self.is_time_processing = false


func _on_TimeLabel_focus_exited():
	self.is_time_processing = true
	self.time = float(self.time_label.get_text())


func _on_TimeLabel_text_entered(new_text):
	self.time_label.release_focus()



#onready var tween = $Tween
#		tween.interpolate_property(
#			self.spirograph, # object: Object,
#			"circle_%02d_%s" % [0, 'radius'],  #property: NodePath,
#			implosion_initial_circle_00_radius, #initial_val: Variant,
#			final_val: Variant,
#			duration: float,
#			trans_type: TransitionType = 0,
#			ease_type: EaseType = 2,
#			delay: float = 0,
#		)
#
#		tween.start()
